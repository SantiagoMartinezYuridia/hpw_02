Grupo = {
    
    "clave": "001",
    "nombre": "ISA",
    "alumnos": [{"num_control":"09161294",   "nombre":"Yuridia Santiago Martínez"},
                {"num_control":"09161293",   "nombre":"Viridiana Santiago Hernandez"},
                {"num_control":"09161096",   "nombre":"GOMEZ MORALES MIRNA LISSETTE"},
                {"num_control":"09161231",   "nombre":"REYES AHUMADA GRACIELA ASTRID"}],
    
    "materias":[{"clave":"01","nombre":"Español"},
                {"clave":"02","nombre":"Matemáticas"},
                {"clave":"03","nombre":"Historia"},
                {"clave":"04","nombre":"Geografía"},
                {"clave":"05","nombre":"Ética"},
                {"clave":"06","nombre":"Programación"},
                {"clave":"07","nombre":"Base de Datos"}],
                
    "profesores":[{"clave":"10100", "nombre":"ARCOS ALVARES ABDIEL SALVADOR"},
                  {"clave":"10101", "nombre":"CAMACHO CORDERO JUAN RAMON"},
                  {"clave":"10102", "nombre":"VAZQUEZ JUAREZ ISRAEL"},
                  {"clave":"10103", "nombre":"JARAMILLO GIL ARTURO"},
                  {"clave":"10104", "nombre":"BASURTO ARRIAGA MARTIN ALBERTO"},
                  {"clave":"10105", "nombre":"SANTANA BEJARANO HILDEGARDO"},
                  {"clave":"10106", "nombre":"SOTO SUAREZ MARGARITA"}],
                  
    "horarios": [{"clave":"1A", "h_inicio":"7:00", "h_final":"8:00","materia":"01","profesor":"10100"},
                {"clave":"2A", "h_inicio":"8:00", "h_final":"9:00","materia":"02","profesor":"10101"},
                {"clave":"3A", "h_inicio":"9:00", "h_final":"10:00","materia":"03","profesor":"10102"},
                {"clave":"4A", "h_inicio":"10:00", "h_final":"11:00","materia":"04","profesor":"10103"},
                {"clave":"5A", "h_inicio":"11:00", "h_final":"12:00","materia":"05","profesor":"10104"},
                {"clave":"6A", "h_inicio":"12:00", "h_final":"13:00","materia":"06","profesor":"10105"},
                {"clave":"7A", "h_inicio":"13:00", "h_final":"14:00","materia":"07","profesor":"10106"}],
}


//------------------.....................-Funciones

function imprimir_lista_alumnos(grup){
    for(var j=0; j<grup["alumnos"].length;j++){
        for(var llave in grup["alumnos"][j]){
            
           // console.log("llave: "+ llave);
            valor=grup["alumnos"][j][llave];
            console.log(llave +": "+ valor);
        }
     
    }
        
    }
    
    
imprimir_lista_alumnos(Grupo);


//------------Imprimir Horarios

function imprimir_lista_horario(grup){
    
     for(var j=0; j<grup["horarios"].length;j++){
           
            h1=grup["horarios"][j]["h_inicio"];
            h2=grup["horarios"][j]["h_final"];
            pr=grup["horarios"][j]["profesor"];
            mt=grup["horarios"][j]["materia"];
            console.log(h1+"-"+h2+"  "+pr+"  "+mt);
         
         
     }
    
}

imprimir_lista_horario(Grupo);

//--------------------------------------------------------------------Busqueda
//---busca alumno

function busca_alu(grup,alu){
    
      for(var j=0; j< grup["alumnos"].length; j++){
            
         if(alu=== grup["alumnos"][j]["num_control"] ){
        return true;   
        }
        } return false;
}

busca_alu(Grupo,"09161095");


//-----Busca profesor

function busca_profe(grup,prof){
    
      for(var j=0; j< grup["profesores"].length; j++){
            
         if(prof=== grup["profesores"][j]["clave"] ){
        return true;   
        }
        } return false;
}

busca_profe(Grupo,"10102");

//-----Busca materia

function busca_materia(grup,mat){
    
      for(var j=0; j< grup["materias"].length; j++){
            
         if(mat=== grup["materias"][j]["clave"] ){
        return true;   
        }
        } return false;
}

busca_materia(Grupo,"02");


//------------------------------------------------Operaciones

//..........insertar alumno

function insert_alu(grup,num,nom){
    var pos=grup["alumnos"].length
    grup["alumnos"][pos]={
        "num_control":num,
        "nombre":nom
    }
    
}

insert_alu(Grupo,"09161040","SALAZAR MENDEZ SAUL");


//.............eliminar alumno
//busca posicion
function busca_al(grup,alu){
    
      for(var j=0; j< grup["alumnos"].length; j++){
            
         if(alu=== grup["alumnos"][j]["num_control"] ){
        return j;   
        }
        }
}

//elimina
function delete_alu(grup,num){
    var pos= busca_al(grup,num);
    grup["alumnos"].splice(pos,1);
   
}

delete_alu(Grupo,"09161293");

//.......................actualiza nombre alumno
function act_alu(grup,num,nuevo){
    var pos= busca_al(grup,num);
    grup["alumnos"][pos]["nombre"]=nuevo;
}

act_alu(Grupo,"09161231","REYES AHUMADA MIREYA");

//............materia 
function busca_mat(grup,mat){
    
      for(var j=0; j< grup["materias"].length; j++){
            
         if(mat=== grup["materias"][j]["clave"] ){
        return j;   
        }
        } 
}

//...busca en que horario lo quiere 

function busca_h(grup,hor){
    
      for(var j=0; j< grup["horarios"].length; j++){
            
         if(hor=== grup["horarios"][j]["h_inicio"] ){
        return j;   
        }
        } 
}


//.............Insertar materia

function insert_mat(grup,clav,nomb,hora){
    var pos=grup["materias"].length
    grup["materias"][pos]={
        "clave":clav,
        "nombre":nomb
    }
    
    var pos2=busca_h(grup,hora);
    
    grup["horarios"][pos2]["materia"]=clav;
    
    
}

insert_mat(Grupo,"00123","Ingles","7:00");

// elimina materia del arreglo de materias y tambien del horario
function delete_mat(grup,mater,h){
    var pos= busca_mat(grup,mater);
    grup["materias"].splice(pos,1);
    
    var pos2=busca_h(grup,h);
    delete grup["horarios"][pos2];
   
}

delete_mat(Grupo,"02","8:00");
Grupo

